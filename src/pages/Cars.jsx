import React from 'react';
import Section from '../components/Section';
import MainLayout from '../layouts/MainLayout';

const Cars = () => {
  return (
    <>
      <MainLayout>
        <Section />
        <section id="searchCars">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-xl-10">
                <div class="search-bar p-4 shadow bg-white">
                  <div class="row">
                    <div class="col-12 col-lg-10">
                      <div class="row">
                        <div class="col-lg-3">
                          <div class="mt-3">
                            <label for="select-driver" class="form-label text-muted">
                              Tipe Driver
                            </label>
                            <select class="form-select" aria-label="Default select example" id="tipeDriver">
                              <option selected disabled>
                                Pilih Tipe Driver
                              </option>
                              <option value="1">Dengan Supir</option>
                              <option value="2">Tanpa Supir</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="mt-3">
                            <label for="date" class="form-label text-muted">
                              Tanggal
                            </label>
                            <input type="date" class="form-control" id="dateSewa" name="date" />
                          </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="mt-3">
                            <label for="pick-up" class="form-label text-muted">
                              Waktu Jemput/Ambil
                            </label>
                            <select class="form-select" aria-label="Default select example" name="time" id="waktuJemput">
                              <option selected disabled>
                                Pilih Waktu
                              </option>
                              <option value="08:00">08:00 &emsp;&emsp; WIB</option>
                              <option value="09:00">09:00 &emsp;&emsp; WIB</option>
                              <option value="10:00">10:00 &emsp;&emsp; WIB</option>
                              <option value="11:00">11:00 &emsp;&emsp; WIB</option>
                              <option value="12:00">12:00 &emsp;&emsp; WIB</option>
                            </select>
                          </div>
                        </div>
                        <div class="col-lg-3">
                          <div class="mt-3">
                            <label for="total" class="form-label text-muted">
                              Jumlah Penumpang (opsional)
                            </label>

                            <input type="number" class="form-control" name="total-passenger" id="jumlahpenumpang" min="0" />
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-12 col-lg-2 position-relative mt-5 pt-4">
                      <button type="button" class="btn btn-success">
                        Cari Mobil
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </MainLayout>
    </>
  );
};

export default Cars;
