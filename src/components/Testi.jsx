import React from 'react';
import ImgTesti1 from '../assets/images/wiya.png';
import ImgTesti2 from '../assets/images/img/img_photo1.png';

const Testi = () => {
  return (
    <section id="section4" class="section4">
      <div class="container-fluid">
        <h1>Testimonial</h1>
        <p>Berbagai review positif dari para pelanggan kami</p>

        <div class="row mt-5">
          <div class="col-6 m-auto">
            <div class="owl-carousel owl-theme">
              <div class="item mb-4">
                <img src={ImgTesti1} alt="img" class="img" />
                <div class="testimoni">
                  <div class="ratingContainer">
                    <i class="bi bi-star-fill checked"></i>
                    <i class="bi bi-star-fill checked"></i>
                    <i class="bi bi-star-fill checked"></i>
                    <i class="bi bi-star-fill checked"></i>
                    <i class="bi bi-star-fill checked"></i>
                  </div>
                  <p class="quote">
                    “Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”
                  </p>
                  <p class="name">John Dee 32, Bromo</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Testi;
